/*
 * Copyright 2021 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package io.javaru.iip.common.ui

import com.intellij.ui.components.JBTextField
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.Toolkit
import java.beans.BeanProperty


@Suppress("unused", "MemberVisibilityCanBePrivate")
class PlaceholderTextField @JvmOverloads constructor(_placeholderText: String? = null, _columns: Int = 0) : JBTextField(_columns)
{
    @set:BeanProperty(bound = false, description = "the placeholder text to display")
    var placeholderText: String? = _placeholderText

    /**
     * The placeholder color to use. If `null`, the value of [getDisabledTextColor]
     * is returned via the getter, and thus used when rendering the placeholder.
     */
    @set:BeanProperty(bound = false, description = "the color to use for the placeholder text")
    var placeholderColor: Color? = null
        get() = if (field != null) field else disabledTextColor

    /**
     * The font to use for rendering the placeholder text. By default, it is identical to the text field's font.
     * or simple way to just change/set the font style, see [placeholderFontStyle]. Setting this
     * `placeholderFont` also modifies the `placeholderFontStyle` and visa-versa.
     */
    @set:BeanProperty(bound = false, description = "the font to use for the placeholder text")
    var placeholderFont: Font = font.deriveFont(font.style)

    /**
     * Convenience property for the font style of the placeholder text. Setting this
     * `placeholderFontStyle` also modifies the [placeholderFont] and visa-versa.
     */
    @set:BeanProperty(bound = false, description = "the font style to use for the placeholder text")
    var placeholderFontStyle: FontStyle
        get() = FontStyle.fromIntValue(placeholderFont.style)
        set(value)
        {
            placeholderFont = placeholderFont.deriveFont(value.intValue)
        }

    /** The horizontal (x-axis left/right) padding for the placeholder text. A positive value moves the placeholder to the right. Default value is 10, a good value for JBTextFields. */
    @set:BeanProperty(bound = false, description = "the horizontal (x-axis left/right) padding for the placeholder text")
    var placeholderPaddingHorizontal: Int = 10

    /** The vertical (y-axis up/down) padding for the placeholder text. A positive value moves the placeholder down. Default value is 0. */
    @set:BeanProperty(bound = false, description = "the vertical (y-axis up/down) padding for the placeholder text; positive value moves down")
    var placeholderPaddingVertical: Int = 0

    override fun paintComponent(graphics: Graphics)
    {
        super.paintComponent(graphics)
        // Only make modifications if a placeholder is set and the user has not entered any text
        if (placeholderText?.isNotBlank() == true && text.isEmpty())
        {
            val graphics2D = graphics as Graphics2D

            if (desktopHints != null)
            {
                graphics2D.setRenderingHints(desktopHints)
            }
            else
            {
                graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)
                graphics2D.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON)
                graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
                graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB)
            }
            graphics2D.color = placeholderColor
            graphics2D.font = placeholderFont
            val x = insets.left + placeholderPaddingHorizontal
            val y = graphics.getFontMetrics().maxAscent + insets.top + placeholderPaddingVertical
            graphics2D.drawString(placeholderText ?: "", x, y)
        }
    }



    companion object
    {
        private const val serialVersionUID = 1967210089246484750L

        private val desktopHints = Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints") as Map<*, *>?
    }
}