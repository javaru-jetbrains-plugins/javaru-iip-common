import org.jetbrains.kotlin.gradle.plugin.getKotlinPluginVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val javaVersion: JavaVersion = JavaVersion.VERSION_11

plugins {
    java
    kotlin("jvm") version "1.5.30"
    id("maven-publish")
    id("signing")
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.0.1"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("nu.studer.credentials") version "3.0"
}

group = "io.javaru.iip.common"
version = "1.1.0-SNAPSHOT"


java {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion

    withSourcesJar()
    withJavadocJar()
}

tasks{
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = javaVersion.toString()
            javaParameters = true
        }
    }
    
    named<Test>("test") {
        useJUnitPlatform()
    }
}


repositories {
    mavenCentral()
    flatDir { dirs("libs") }
}


dependencyManagement {
    imports {
        mavenBom("org.jetbrains.kotlin:kotlin-bom:${project.getKotlinPluginVersion()}")
        mavenBom("org.junit:junit-bom:5.7.2")
    }

    dependencies {
        dependency("org.apache.commons:commons-lang3:3.12.0")
    }
}

dependencies {
    compileOnly("intellij-idea-jars:platform-api:2021.1")
    compileOnly("intellij-idea-jars:platform-impl:2021.1")
    compileOnly("intellij-idea-jars:util:2021.1")

    implementation("org.apache.commons:commons-lang3")
    
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            // https://docs.gradle.org/current/userguide/publishing_maven.html
            artifactId = project.name
            from(components["java"])
            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }
            pom {
                name.set("Javaru IntelliJ IDEA Plugin Common")
                description.set("Common lib used for development of IntelliJ IDEA plugins written by Javaru.")
                url.set("https://gitlab.com/javaru-jetbrains-plugins/javaru-iip-common")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com:javaru-jetbrains-plugins/javaru-iip-common.git")
                    developerConnection.set("scm:git:ssh://gitlab.com:javaru-jetbrains-plugins/javaru-iip-common.git")
                    url.set("https://gitlab.com/javaru-jetbrains-plugins/javaru-iip-common")
                }
                developers { 
                    developer {
                        id.set("Javaru")
                        name.set("Javaru")
                        email.set("obfuscated@example.com")
                        organization.set("Javaru")
                        organizationUrl.set("https://gitlab.com/javaru")
                    }
                }
            }
            repositories {
                maven {
                    // change to point to your repo, e.g. http://my.org/repo
                    //url = uri(layout.buildDirectory.dir("repo"))
                    val releasesRepoUrl = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
                    val snapshotsRepoUrl = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
                    url = if (version.toString().contains("SNAPSHOT", ignoreCase = true)) snapshotsRepoUrl else releasesRepoUrl
                    credentials { 
                        val credentials: nu.studer.gradle.credentials.domain.CredentialsContainer by project.extra
                        username = System.getenv("OSSRH_USERNAME") ?: credentials.forKey("ossrhUsername") ?: "NOT_FOUND".also { logger.error("Could not find 'ossrhUsername'") }
                        password = System.getenv("OSSRH_PASSWORD") ?: credentials.forKey("ossrhPassword") ?: "NOT_FOUND".also { logger.error("Could not find 'ossrhPassword'") }
                    }
                }
            }
        }
    }
}

signing {
    // The "signing" block must come after the "publishing.publications" block so our "mavenJava" definition is found
    // https://docs.gradle.org/current/userguide/publishing_signing.html
    // https://docs.gradle.org/current/userguide/signing_plugin.html#sec:signatory_credentials
    sign(publishing.publications["mavenJava"])
    
}

idea {
// https://github.com/JetBrains/gradle-idea-ext-plugin/wiki
    module {
        // Download and attach Source & Javadoc JARs for dependencies when available
        isDownloadSources = true
        isDownloadJavadoc = true
    }
    project {
        (this as ExtensionAware)
        configure<org.jetbrains.gradle.ext.ProjectSettings> {
            this as ExtensionAware
            configure<org.jetbrains.gradle.ext.EncodingConfiguration> {
                // setting EncodingConfiguration requires IDEA 2019.1+
                this.encoding = "UTF-8"
                this.bomPolicy = org.jetbrains.gradle.ext.EncodingConfiguration.BomPolicy.WITH_NO_BOM
                properties {
                    encoding = "ISO-8859-1"
                    transparentNativeToAsciiConversion = false
                }
            }

            configure<org.jetbrains.gradle.ext.CopyrightConfiguration> {
                useDefault = "Apache 2 -- 2021 inception"
                profiles {
                    create("Apache 2 -- 2021 inception") {
                        this.keyword = "Copyright"
                        this.notice = """
                                    #set( ${'$'}inceptionYear = 2021 )
                                    Copyright ${'$'}inceptionYear#if(${'$'}today.year!=${'$'}inceptionYear)-${'$'}today.year#end the original author or authors.
                                    
                                        Licensed under the Apache License, Version 2.0 (the "License");
                                        you may not use this file except in compliance with the License.
                                        You may obtain a copy of the License at
                                    
                                          https://www.apache.org/licenses/LICENSE-2.0
                                        
                                        Unless required by applicable law or agreed to in writing, software
                                        distributed under the License is distributed on an "AS IS" BASIS,
                                        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
                                        See the License for the specific language governing permissions and
                                        limitations under the License.
                                """.trimIndent()
                    }
                }
            }
        }
    }
}
